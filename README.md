# README #

## Proyecto 1 de CI-5437 *Inteligencia Artificial I* ##

### Búsqueda Heurística ###

> Objetivo:
>
> El objetivo del proyecto es aprender sobre el modelo
> de  espacio  de  estados  y  sobre  los  diferentes
> algoritmos  de  búsqueda  heurística.    No  sólo  se
> evaluará la correctitud de la implementación;  es
> importante  que  los  algoritmos  sean  eficientes  y
> puedan resolver los problemas propuestos en los
> tiempos estipulados.

### Problemas: ###
* N-puzzels: 4x4 , 5x5
* Cubo de Rubik: 2x2x2, 3x3x3
* Top-spin: 12-4, 14-4, 16-4 y 17-4
* Torre  de  Hanoi  con  4  astas:   12,  14,  16  y  18 discos

****
## Autores ##
* Enrique Iglesias *enrique@ldc.usb.ve*

* Jesus Adolfo Parra *adolfo@ldc.usb.ve*