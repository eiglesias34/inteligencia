/*
 * =====================================================================================
 *
 *       Filename:  Algoritmos.hpp
 *
 *    Description:  Firmas de los Algoritmos usados
 *
 *        Version:  1.0
 *        Created:  13/05/15 01:16:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Jesus Adolfo Parra Parra (Adolfo), adolfo@ldc.usb.ve
 *   Organization:  Universidad Simon Bolivar
 *
 * =====================================================================================
 */
#include <iostream>
#include <fstream>
#include <queue> 
#include <unistd.h>
#include "Estado.hpp"
#include <cstring>
#include <stack>
#include <ctime>
#define NEGRO 0
#define BLANCO 1
#define GRIS 2
#define PRG_NAME "CuboDeRubik"

using namespace std;

typedef queue<Estado*> Cola;
typedef stack<Estado*> Pila;

 
Estado * bfs_ddd(state_t * incio);

Estado * ucs_ddd( state_t * inicio);

Par bound_dfs( Estado *inicio, int bound);

Estado * dfid (state_t * inicio);

void imprimir( ofstream *file, Estado *p);

void ayuda(char * nombre);
