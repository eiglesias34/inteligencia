#include "Algoritmos.hpp"
using namespace std;

int main(int argc, char *argv[]) {
	Estado *p;
	char opcion;
	ofstream file;
    ifstream f_entrada;
    state_t inicio;
    char string[300];
    bool entrada = false;
    bool salida = false;
	int opt = 3;
	clock_t comienzo,final;
	extern long int nodos_generados;
	double segundos;

	while ((opcion = getopt(argc,argv,"f:hi:e:dbu")) != -1 ) {
		switch (opcion) {
			case 'h' :	ayuda(argv[0]);
				break;
			case 'f': {
                          file.open(optarg);
                          salida = true;
                      }
				break;
			case 'i': {
                        if ( (strncpy(string,optarg,300)) == NULL ) {
                            cout << "Fallo al leer el estado inicial" << endl;
		                    exit(-1);
                       }
                        entrada = true;
                        if (read_state (string,&inicio) <=0 ) {
                            cout << "Fallo al leer el estado inicial" << endl;
		                    exit(-1);
                        }
                      }
				break;
            case 'e': 
                {   f_entrada.open(optarg);
                     entrada = true;
                     if ( f_entrada.fail() || !(f_entrada.is_open())) {
		                cout << "Fallo al abrir el archivo, se detiene la ejecucion" << endl;
		                exit(-1);
	                }   
                }
                break;
			case 'b' : opt = 1;
				break;
			case 'u' : opt = 2;
				break;
			case 'd' : opt = 3;
				break;
			default:  ayuda(argv[0]);
				break;
			}
	}				

    if ( !(entrada && salida)) {
         cout << "Se necesita una entrada y el fichero de salida" << endl;
		exit(-1);
    }

	if (file.fail() || !(file.is_open())) {
		cout << "Fallo al abrir el archivo, se detiene la ejecucion" << endl;
		exit(-1);
	}
    
    if (f_entrada.is_open()) {
        while (true) {
			f_entrada.getline(string,300);
			if (f_entrada.eof())
				break;
        	if (read_state (string,&inicio) <=0 ) {
            	cout << "Fallo al leer el estado inicial" << endl;
		    	exit(-1);
        	}
			comienzo = clock();
			switch(opt) {
				case 1 : p = bfs_ddd(&inicio);
					break;
				case 2:  p = ucs_ddd(&inicio);
					break;
				case 3: p = dfid(&inicio);
					break;
			}
			final = clock();
			segundos = (double) (final - comienzo) / (double)CLOCKS_PER_SEC;
            file << string << " : " << " - " << p->get_distancia() << " ";
            file << nodos_generados << " " << segundos << endl;     
		}
    }

    file.close();
}
