/*
 * =====================================================================================
 *
 *       Filename:  Algoritmos.cpp
 *
 *    Description:  implementacion de los algoritmos usados
 *
 *        Version:  1.0
 *        Created:  13/05/15 01:03:54
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Jesus Adolfo Parra Parra (Adolfo), adolfo@ldc.usb.ve
 *   Organization:  Universidad Simon Bolivar
 *
 * =====================================================================================
 */
#include "Algoritmos.hpp"

long int nodos_generados;


Estado * bfs_ddd(state_t * inicio) {
    state_map_t *tabla_hash = new_state_map();
    Cola nodos;
    nodos.push(new Estado(inicio,init_history,NULL,0));
    state_map_add(tabla_hash,inicio,GRIS);
    Estado *p;
   	state_t *hijo;
	ruleid_iterator_t iterator;
    int *color;
    int ruleid;
	int dist;
	nodos_generados = 1;

    while (! nodos.empty()) {
    		p = nodos.front();
    		nodos.pop();
            if (is_goal(p->get_estado())) { 
                return p;
                break;
            }

        	init_fwd_iter(&iterator,p->get_estado());
    		while ( (ruleid = next_ruleid(&iterator)) >=0 ) {
    			    if (! fwd_rule_valid_for_history(p->get_historia(),ruleid) )
    					continue;
    				hijo = (state_t *) malloc (sizeof(state_t));
    				apply_fwd_rule(ruleid,p->get_estado(),hijo);
                    color = state_map_get(tabla_hash,hijo);
                    if ( color == NULL || *color == GRIS) {
                        state_map_add(tabla_hash,hijo,GRIS);
                       	nodos.push(new Estado(hijo, next_fwd_history(p->get_historia(),ruleid),p,p->get_distancia()+1));
						nodos_generados += 1;
                    }

    		}
            state_map_add(tabla_hash,p->get_estado(),NEGRO);
	}
}

Estado * ucs_ddd(state_t * inicio) {

    state_map_t *tabla_hash = new_state_map();
    state_map_t *tabla_distancias = new_state_map();
    priority_queue<Estado*,vector<Estado*>,EstadoComp> nodos;
    nodos.push(new Estado(inicio,init_history,NULL,0));
    state_map_add(tabla_hash,inicio,GRIS);
    state_map_add(tabla_distancias,inicio,0);
    Estado *p;
   	state_t *hijo;
	ruleid_iterator_t iterator;
    int *color;
    int ruleid;
    int dist;
	nodos_generados = 1;

    while (! nodos.empty()) {
    		p = nodos.top();
    		nodos.pop();
            if (is_goal(p->get_estado())) { 
                return p;
                break;
            }

        	init_fwd_iter(&iterator,p->get_estado());
    		while ( (ruleid = next_ruleid(&iterator)) >=0 ) {
    			    if (! fwd_rule_valid_for_history(p->get_historia(),ruleid) )
    					continue;
    				hijo = (state_t *) malloc (sizeof(state_t));
    				dist = *state_map_get(tabla_distancias,p->get_estado()) + get_fwd_rule_cost(ruleid);
                    apply_fwd_rule(ruleid,p->get_estado(),hijo);
                    color = state_map_get(tabla_hash,hijo);
                    if ( color == NULL || (dist < *state_map_get(tabla_distancias,hijo))) {
                        state_map_add(tabla_hash,hijo,GRIS);
                        state_map_add(tabla_distancias,hijo,dist);
                 		nodos.push(new Estado(hijo, next_fwd_history(p->get_historia(),ruleid),p,dist));
						nodos_generados += 1;
                    }
    		}
            state_map_add(tabla_hash,p->get_estado(),NEGRO);
	}
}

Par bound_dfs (Estado *inicio ,int bound); 
Estado * dfid (state_t * inicio) {

    Estado *p;
   	state_t *hijo;
	ruleid_iterator_t iterator;
    int ruleid;
    int dist;
    int bound = 0;
    p = new Estado(inicio,init_history,NULL);
	nodos_generados = 1;
    Par res;

    while (true) {
        res = bound_dfs(p,bound);
        if (res.Nodo != NULL ) 
            return res.Nodo;
        bound = res.distancia;
    }
}

Par bound_dfs (Estado *inicio ,int bound) {

    Estado *p;
   	state_t *hijo;
	ruleid_iterator_t iterator;
    int ruleid;
    int dist = -1;
    int distancia;
    Par res;
	Estado *hijoE;

    if (inicio->get_distancia() > bound){
        res.Nodo = NULL;
        res.distancia = inicio->get_distancia();
        return res;
    }

    if (is_goal(inicio->get_estado())){
        res.Nodo = inicio;
        res.distancia = inicio->get_distancia();
        return res;
     }

   	init_fwd_iter(&iterator,inicio->get_estado());
    while ( (ruleid = next_ruleid(&iterator)) >=0 ) {
         if (! fwd_rule_valid_for_history(inicio->get_historia(),ruleid) )
        			continue;
  	    hijo = (state_t *) malloc (sizeof(state_t));
        apply_fwd_rule(ruleid,inicio->get_estado(),hijo);
        distancia = inicio->get_distancia() + get_fwd_rule_cost(ruleid);
        hijoE = new Estado(hijo,next_fwd_history(inicio->get_historia(),ruleid),inicio,distancia);
		nodos_generados += 1;
        res = bound_dfs(hijoE,bound);
        if (res.Nodo != NULL) 
            return res;
        if (dist < 0) {
            res.Nodo = NULL;
            dist = res.distancia;
        }
        else if (dist > res.distancia) {
            dist = res.distancia;
        }

        free(hijo);
        delete (hijoE);
    
    }
    res.Nodo = NULL;
    res.distancia = dist;
    return res; 
}



void imprimir( ofstream *file, Estado * p) {     
    Pila pila;
    char string[300];
    while ( p->get_padre() != NULL) {
        pila.push(p);
        p = p->get_padre();
    }
    pila.push(p);
    *file << "pasos para resolverlo: \n" << endl;
    while (!pila.empty()) {

        sprint_state ( &string[0],300,pila.top()->get_estado());
        pila.pop();
        *file << string << endl;
    }
}


void ayuda(char * nombre) {
	cout << PRG_NAME << ": modo de uso" << endl ;
	cout << PRG_NAME <<" -f [archivo] -h -i -e -b -d -u" << endl ;
	cout << "Opciones: \n-f [archivo]: indica que en 'archivo' se guardaran los datos" << endl;
	cout << "-h : muestra esta ayuda" << endl;
    cout << "-i : Indica el estado inicial" << endl;
	cout << "-e : Indica el archivo donde se leera el estado inicial" << endl;
    cout << "Si se usa al mismo tiempo '-e' y '-i', el archivo tiene preferencia" << endl;
	cout << "-b : Se usara el algoritmo de BFS con DDD" << endl;
    cout << "-d : Se usara DFID (usado por defecto)" << endl;
	cout << "-u : Se usara UCS con DDD" << endl;
	cout << "Si se usa al mismo tiempo varios algoritmo, solo se ejecutara el ultimo" << endl;
	
	exit(0);
}
