El código que genera el árbol de busqueda se llama npuzzle.cpp.
Este requiere que se le pase dos parámetros. El primero indica que 
árbol se desea general:
0: Con duplicados.
1: Sin duplicados.
El segundo indica la profundidad.

Se compila corriendo el Makefile.

El archivo SalidaNoDuplicados.txt ya contiene una tabla con la cantidad de nodos
por nivel, haciendo parent pruning.

El archivo SalidaDuplicados.txt ya contiene una tabla con la cantidad de nodos
por nivel, sin parent pruning.