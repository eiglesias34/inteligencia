/*
 * Autor: Enrique Iglesias y Jesus Parra
 * Fecha: 
 */

#include <iostream>
#include <queue>
#include <fstream>
using namespace std;

void bfs_rep(int nivel) {
    state_t goal;
    state_t* child;
    state_t* n;
    int p; 
    ruleid_iterator_t iter;
    int ruleid; 
    priority_queue<state_t*> q, q2;
    first_goal_state(&goal,&p);
    q.push(&goal);
    ofstream file;
    file.open("SalidaDuplicados.txt",fstream::out);
    file << "Número de nodos en el nivel 0 son: " << q.size() <<"\n";
    for (int i = 1; i < nivel; ++i)
    {
        if (!q.empty())
        {
            while (! q.empty() ){
                n = q.top();
                q.pop();
                init_fwd_iter(&iter, n);
                while ( ( ruleid = next_ruleid( &iter ) ) >= 0 ){
                    child = (state_t*) malloc (sizeof(state_t));
                    apply_fwd_rule( ruleid, n, child );
                    q2.push(child);
                };
            };
            file << "Número de nodos en el nivel " << i << " son: " << q2.size() <<"\n";
        }
        else{
            while (!q2.empty()){
                n = q2.top();
                q2.pop();
                init_fwd_iter( &iter, n);
                while ( ( ruleid = next_ruleid( &iter ) ) >= 0 ){
                    child = (state_t*) malloc (sizeof(state_t));
                    apply_fwd_rule( ruleid, n, child );
                    q.push(child);
                };
            };
            file << "Número de nodos en el nivel " << i << " son: " << q.size() <<"\n";
        };
    };
    free(child);
    file.close();
};

void bfs_sin_rep(int nivel) {
    state_t goal;
    state_t* child;
    state_t* n;
    int p; 
    ruleid_iterator_t iter;
    int ruleid; 
    priority_queue<state_t*> q, q2;
    priority_queue<int> hist1,hist2; 
    first_goal_state(&goal,&p);
    q.push(&goal);
    hist1.push(init_history); 
    ofstream file;
    file.open("SalidaNoDuplicados.txt",fstream::out);
    file << "Número de nodos en el nivel 0 son: " << q.size() <<"\n";
    for (int i = 1; i < nivel; ++i)
    {
        if (!q.empty())
        {
            while (! q.empty() ){
                n = q.top();
                int hist = hist1.top();
                hist1.pop();
                q.pop();
                init_fwd_iter(&iter, n);
                while ( ( ruleid = next_ruleid( &iter ) ) >= 0 ){
                    child = (state_t*) malloc (sizeof(state_t));
                    apply_fwd_rule( ruleid, n, child );
                    if (fwd_rule_valid_for_history(hist, ruleid) != 0)
                    {
                        q2.push(child);
                        hist2.push(next_fwd_history(hist, ruleid));
                    }
                };
            };
            file << "Número de nodos en el nivel " << i << " son: " << q2.size() <<"\n";
        }
        else{
            while (!q2.empty()){
                n = q2.top();
                int hist = hist2.top();
                hist2.pop();
                q2.pop();
                init_fwd_iter( &iter, n);
                while ( ( ruleid = next_ruleid( &iter ) ) >= 0 ){
                    child = (state_t*) malloc (sizeof(state_t));
                    apply_fwd_rule( ruleid, n, child );
                    if (fwd_rule_valid_for_history(hist, ruleid) != 0)
                    {
                        q.push(child);
                        hist1.push(next_fwd_history(hist, ruleid));
                    }
                };
            };
            file << "Número de nodos en el nivel " << i << " son: " << q.size() <<"\n";
        };
    };
    file.close();
};

int main(int argc, char const *argv[])
{
    
    if (atoi(argv[1]) == 0)
    {
        bfs_rep(atoi(argv[2]));
    }
    else{
        bfs_sin_rep(atoi(argv[2]));
    };
    return 0;
}