#include "Algoritmos.hpp"
using namespace std;

int main(int argc, char *argv[]) {
	Estado *p;
	char opcion;
	ofstream file;
    ifstream f_entrada;
    state_t inicio;
    char string[100];
    bool entrada = false;
    bool salida = false;
	bool manhatthan = true;
	bool a_ddd = true;
    clock_t comienzo, final;
    double segundos;
    extern long int nodos_generados;

	while ((opcion = getopt(argc,argv,"f:hi:e:po")) != -1 ) {
		switch (opcion) {
			case 'h' :	ayuda(argv[0]);
				break;
			case 'f': {
                          file.open(optarg);
                          salida = true;
                      }
				break;
			case 'i': {
                        if ( (strncpy(string,optarg,100)) == NULL ) {
                            cout << "Fallo al leer el estado inicial" << endl;
		                    exit(-1);
                       }
                        entrada = true;
                        if (read_state (string,&inicio) <=0 ) {
                            cout << "Fallo al leer el estado inicial" << endl;
		                    exit(-1);
                        }
                      }
				break;
            case 'e': 
                {   f_entrada.open(optarg);
                     entrada = true;
                     if ( f_entrada.fail() || !(f_entrada.is_open())) {
		                cout << "Fallo al abrir el archivo, se detiene la ejecucion" << endl;
		                exit(-1);
	                }   
                }
                break;
			case 'p' : manhatthan = false;
				break;
			case 'o' : a_ddd = false;
				break;

			default:  ayuda(argv[0]);
				break;
			}
	}				

    if ( !(entrada && salida)) {
         cout << "Se necesita una entrada y el fichero de salida" << endl;
		exit(-1);
    }

	if (file.fail() || !(file.is_open())) {
		cout << "Fallo al abrir el archivo, se detiene la ejecucion" << endl;
		exit(-1);
	}
    if (manhatthan) {
    	inicializar_pdb_manhatthan();
    }
    else {
    	inicializar_pdb_54();
    }

    if (f_entrada.is_open()) {
        while(true) {
            f_entrada.getline(string,100);
            if (f_entrada.eof())
                break;
            if (read_state (string,&inicio) <=0 ) {
                cout << "Fallo al leer el estado inicial" << endl;
    		    exit(-1);
            }
        
        
            comienzo = clock();    
        	if (a_ddd) {
        		p = addd(&inicio, manhatthan);
        	}
        	else {
        		p = ida(&inicio, manhatthan);
        	}
            final = clock();
            segundos = (double) (final - comienzo) / (double)CLOCKS_PER_SEC;
            file << string << " : " << heuristica(&inicio,manhatthan) <<" " << p->get_distancia() << " ";
            file << nodos_generados << " " << segundos << endl;    
        }
    file.close();
    }
}
