/*
 * =====================================================================================
 *
 *       Filename:  Algoritmos.hpp
 *
 *    Description:  Firmas de los Algoritmos usados
 *
 *        Version:  1.0
 *        Created:  13/05/15 01:16:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Jesus Adolfo Parra Parra (Adolfo), adolfo@ldc.usb.ve
 *   Organization:  Universidad Simon Bolivar
 *
 * =====================================================================================
 */
#include <iostream>
#include <fstream>
#include <queue> 
#include <unistd.h>
#include "Estado.hpp"
#include <cstring>
#include <stack>
#include <cstdio>
#include <ctime>
#define NEGRO 0
#define BLANCO 1
#define GRIS 2
#define PRG_NAME "Npuzzle"

using namespace std;

typedef queue<Estado*> Cola;
typedef stack<Estado*> Pila;

 
Estado * addd( state_t * inicio, bool manhatthan);

Par f_bound_dfs( Estado *inicio, int bound, bool manhatthan);

Estado * dfid (state_t * inicio);

void imprimir( ofstream *file, Estado *p);

void ayuda(char * nombre);

void inicializar_pdb_manhatthan();

int heuristica(state_t * estado, bool manhatthan);

void inicializar_pdb_54();

Estado * ida(state_t * estado, bool manhatthan);

