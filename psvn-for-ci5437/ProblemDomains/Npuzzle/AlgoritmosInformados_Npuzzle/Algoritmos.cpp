/*
 * #inicio <ctime>
 * =====================================================================================
 *
 *       Filename:  Algoritmos.cpp
 *
 *    Description:  implementacion de los algoritmos usados
 *
 *        Version:  1.0
 *        Created:  13/05/15 01:03:54
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Jesus Adolfo Parra Parra (Adolfo), adolfo@ldc.usb.ve
 *   Organization:  Universidad Simon Bolivar
 *
 * =====================================================================================
 */
#include "Algoritmos.hpp"

//Variables globales, son las abstracciones y las tablas de hash
    long int nodos_generados;    

	abstraction_t *abs_1;
	abstraction_t *abs_2;
	abstraction_t *abs_3;
	abstraction_t *abs_4;
	abstraction_t *abs_5;
	abstraction_t *abs_6;
	abstraction_t *abs_7;
	abstraction_t *abs_8;
	abstraction_t *abs_9;
	abstraction_t *abs_10;
	abstraction_t *abs_11;
	abstraction_t *abs_12;
	abstraction_t *abs_13;
	abstraction_t *abs_14;
	abstraction_t *abs_15;
    abstraction_t *abs_16;
	abstraction_t *abs_17;
	abstraction_t *abs_18;
	abstraction_t *abs_19;
	abstraction_t *abs_20;
	abstraction_t *abs_21;
	abstraction_t *abs_22;
	abstraction_t *abs_23;
	abstraction_t *abs_24;

	FILE *pdb_1;
	FILE *pdb_2;
	FILE *pdb_3;
	FILE *pdb_4;
	FILE *pdb_5;
	FILE *pdb_6;
	FILE *pdb_7;
	FILE *pdb_8;
	FILE *pdb_9;
	FILE *pdb_10;
	FILE *pdb_11;
	FILE *pdb_12;
	FILE *pdb_13;
	FILE *pdb_14;
	FILE *pdb_15;
    FILE *pdb_16;
	FILE *pdb_17;
	FILE *pdb_18;
	FILE *pdb_19;
	FILE *pdb_20;
	FILE *pdb_21;
	FILE *pdb_22;
	FILE *pdb_23;
	FILE *pdb_24;

	state_map_t * t_1;
	state_map_t * t_2;
	state_map_t * t_3;
	state_map_t * t_4;
	state_map_t * t_5;
	state_map_t * t_6;
	state_map_t * t_7;
	state_map_t * t_8;
	state_map_t * t_9;
	state_map_t * t_10;
	state_map_t * t_11;
	state_map_t * t_12;
	state_map_t * t_13;
	state_map_t * t_14;
	state_map_t * t_15;
    state_map_t * t_16;
	state_map_t * t_17;
	state_map_t * t_18;
	state_map_t * t_19;
	state_map_t * t_20;
	state_map_t * t_21;
	state_map_t * t_22;
	state_map_t * t_23;
	state_map_t * t_24;



void inicializar_pdb_manhatthan() {
	if (VERSION && TAM) {
		 abs_1 = read_abstraction_from_file("abstracciones/4a1.abst");
		 abs_2 = read_abstraction_from_file("abstracciones/4a2.abst");
		 abs_3 = read_abstraction_from_file("abstracciones/4a3.abst");
		 abs_4 = read_abstraction_from_file("abstracciones/4a4.abst");
		 abs_5 = read_abstraction_from_file("abstracciones/4a5.abst");
		 abs_6 = read_abstraction_from_file("abstracciones/4a6.abst");
		 abs_7 = read_abstraction_from_file("abstracciones/4a7.abst");
		 abs_8 = read_abstraction_from_file("abstracciones/4a8.abst");
		 abs_9 = read_abstraction_from_file("abstracciones/4a9.abst");
		 abs_10 = read_abstraction_from_file("abstracciones/4a10.abst");
		 abs_11 = read_abstraction_from_file("abstracciones/4a11.abst");
		 abs_12 = read_abstraction_from_file("abstracciones/4a12.abst");
		 abs_13 = read_abstraction_from_file("abstracciones/4a13.abst");
		 abs_14 = read_abstraction_from_file("abstracciones/4a14.abst");
		 abs_15 = read_abstraction_from_file("abstracciones/4a15.abst");

		 pdb_1 = fopen ("abstracciones/4a(1)_map_state","r");
		 pdb_2 = fopen ("abstracciones/4a(2)_map_state","r");
		 pdb_3 = fopen ("abstracciones/4a(3)_map_state","r");
		 pdb_4 = fopen ("abstracciones/4a(4)_map_state","r");
		 pdb_5 = fopen ("abstracciones/4a(5)_map_state","r");
		 pdb_6 = fopen ("abstracciones/4a(6)_map_state","r");
		 pdb_7 = fopen ("abstracciones/4a(7)_map_state","r");
		 pdb_8 = fopen ("abstracciones/4a(8)_map_state","r");
		 pdb_9 = fopen ("abstracciones/4a(9)_map_state","r");
		 pdb_10 = fopen ("abstracciones/4a(10)_map_state","r");
		 pdb_11 = fopen ("abstracciones/4a(11)_map_state","r");
		 pdb_12 = fopen ("abstracciones/4a(12)_map_state","r");
		 pdb_13 = fopen ("abstracciones/4a(13)_map_state","r");
		 pdb_14 = fopen ("abstracciones/4a(14)_map_state","r");
		 pdb_15 = fopen ("abstracciones/4a(15)_map_state","r");

		}
	else if (!VERSION && TAM) {
		 abs_1 = read_abstraction_from_file("abstracciones/4a1.abst");
		 abs_2 = read_abstraction_from_file("abstracciones/4a2.abst");
		 abs_3 = read_abstraction_from_file("abstracciones/4a3.abst");
		 abs_4 = read_abstraction_from_file("abstracciones/4a4.abst");
		 abs_5 = read_abstraction_from_file("abstracciones/4a5.abst");
		 abs_6 = read_abstraction_from_file("abstracciones/4a6.abst");
		 abs_7 = read_abstraction_from_file("abstracciones/4a7.abst");
		 abs_8 = read_abstraction_from_file("abstracciones/4a8.abst");
		 abs_9 = read_abstraction_from_file("abstracciones/4a9.abst");
		 abs_10 = read_abstraction_from_file("abstracciones/4a10.abst");
		 abs_11 = read_abstraction_from_file("abstracciones/4a11.abst");
		 abs_12 = read_abstraction_from_file("abstracciones/4a12.abst");
		 abs_13 = read_abstraction_from_file("abstracciones/4a13.abst");
		 abs_14 = read_abstraction_from_file("abstracciones/4a14.abst");
		 abs_15 = read_abstraction_from_file("abstracciones/4a15.abst");

		 pdb_1 = fopen ("abstracciones/4b(1)_map_state","r");
		 pdb_2 = fopen ("abstracciones/4b(2)_map_state","r");
		 pdb_3 = fopen ("abstracciones/4b(3)_map_state","r");
		 pdb_4 = fopen ("abstracciones/4b(4)_map_state","r");
		 pdb_5 = fopen ("abstracciones/4b(5)_map_state","r");
		 pdb_6 = fopen ("abstracciones/4b(6)_map_state","r");
		 pdb_7 = fopen ("abstracciones/4b(7)_map_state","r");
		 pdb_8 = fopen ("abstracciones/4b(8)_map_state","r");
		 pdb_9 = fopen ("abstracciones/4b(9)_map_state","r");
		 pdb_10 = fopen ("abstracciones/4b(10)_map_state","r");
		 pdb_11 = fopen ("abstracciones/4b(11)_map_state","r");
		 pdb_12 = fopen ("abstracciones/4b(12)_map_state","r");
		 pdb_13 = fopen ("abstracciones/4b(13)_map_state","r");
		 pdb_14 = fopen ("abstracciones/4b(14)_map_state","r");
		 pdb_15 = fopen ("abstracciones/4b(15)_map_state","r");

		}
	else if (VERSION && !TAM) {
		 abs_1 = read_abstraction_from_file("abstracciones/5a1.abst");
		 abs_2 = read_abstraction_from_file("abstracciones/5a2.abst");
		 abs_3 = read_abstraction_from_file("abstracciones/5a3.abst");
		 abs_4 = read_abstraction_from_file("abstracciones/5a4.abst");
		 abs_5 = read_abstraction_from_file("abstracciones/5a5.abst");
		 abs_6 = read_abstraction_from_file("abstracciones/5a6.abst");
		 abs_7 = read_abstraction_from_file("abstracciones/5a7.abst");
		 abs_8 = read_abstraction_from_file("abstracciones/5a8.abst");
		 abs_9 = read_abstraction_from_file("abstracciones/5a9.abst");
		 abs_10 = read_abstraction_from_file("abstracciones/5a10.abst");
		 abs_11 = read_abstraction_from_file("abstracciones/5a11.abst");
		 abs_12 = read_abstraction_from_file("abstracciones/5a12.abst");
		 abs_13 = read_abstraction_from_file("abstracciones/5a13.abst");
		 abs_14 = read_abstraction_from_file("abstracciones/5a14.abst");
		 abs_15 = read_abstraction_from_file("abstracciones/5a15.abst");
         abs_16 = read_abstraction_from_file("abstracciones/5a16.abst");
		 abs_17 = read_abstraction_from_file("abstracciones/5a17.abst");
		 abs_18 = read_abstraction_from_file("abstracciones/5a18.abst");
		 abs_19 = read_abstraction_from_file("abstracciones/5a19.abst");
		 abs_20 = read_abstraction_from_file("abstracciones/5a20.abst");
		 abs_21 = read_abstraction_from_file("abstracciones/5a21.abst");
		 abs_22 = read_abstraction_from_file("abstracciones/5a22.abst");
		 abs_23 = read_abstraction_from_file("abstracciones/5a23.abst");
		 abs_24 = read_abstraction_from_file("abstracciones/5a24.abst");


		 pdb_1 = fopen ("abstracciones/5a(1)_map_state","r");
		 pdb_2 = fopen ("abstracciones/5a(2)_map_state","r");
		 pdb_3 = fopen ("abstracciones/5a(3)_map_state","r");
		 pdb_4 = fopen ("abstracciones/5a(4)_map_state","r");
		 pdb_5 = fopen ("abstracciones/5a(5)_map_state","r");
		 pdb_6 = fopen ("abstracciones/5a(6)_map_state","r");
		 pdb_7 = fopen ("abstracciones/5a(7)_map_state","r");
		 pdb_8 = fopen ("abstracciones/5a(8)_map_state","r");
		 pdb_9 = fopen ("abstracciones/5a(9)_map_state","r");
		 pdb_10 = fopen ("abstracciones/5a(10)_map_state","r");
		 pdb_11 = fopen ("abstracciones/5a(11)_map_state","r");
		 pdb_12 = fopen ("abstracciones/5a(12)_map_state","r");
		 pdb_13 = fopen ("abstracciones/5a(13)_map_state","r");
		 pdb_14 = fopen ("abstracciones/5a(14)_map_state","r");
		 pdb_15 = fopen ("abstracciones/5a(15)_map_state","r");
         pdb_16 = fopen ("abstracciones/5a(16)_map_state","r");
		 pdb_17 = fopen ("abstracciones/5a(17)_map_state","r");
		 pdb_18 = fopen ("abstracciones/5a(18)_map_state","r");
		 pdb_19 = fopen ("abstracciones/5a(19)_map_state","r");
		 pdb_20 = fopen ("abstracciones/5a(20)_map_state","r");
		 pdb_21 = fopen ("abstracciones/5a(21)_map_state","r");
		 pdb_22 = fopen ("abstracciones/5a(22)_map_state","r");
		 pdb_23 = fopen ("abstracciones/5a(23)_map_state","r");
		 pdb_24 = fopen ("abstracciones/5a(24)_map_state","r");

	}
	else {
		 abs_1 = read_abstraction_from_file("abstracciones/5b1.abst");
		 abs_2 = read_abstraction_from_file("abstracciones/5b2.abst");
		 abs_3 = read_abstraction_from_file("abstracciones/5b3.abst");
		 abs_4 = read_abstraction_from_file("abstracciones/5b4.abst");
		 abs_5 = read_abstraction_from_file("abstracciones/5b5.abst");
		 abs_6 = read_abstraction_from_file("abstracciones/5b6.abst");
		 abs_7 = read_abstraction_from_file("abstracciones/5b7.abst");
		 abs_8 = read_abstraction_from_file("abstracciones/5b8.abst");
		 abs_9 = read_abstraction_from_file("abstracciones/5b9.abst");
		 abs_10 = read_abstraction_from_file("abstracciones/5b10.abst");
		 abs_11 = read_abstraction_from_file("abstracciones/5b11.abst");
		 abs_12 = read_abstraction_from_file("abstracciones/5b12.abst");
		 abs_13 = read_abstraction_from_file("abstracciones/5b13.abst");
		 abs_14 = read_abstraction_from_file("abstracciones/5b14.abst");
		 abs_15 = read_abstraction_from_file("abstracciones/5b15.abst");
         abs_16 = read_abstraction_from_file("abstracciones/5b16.abst");
		 abs_17 = read_abstraction_from_file("abstracciones/5b17.abst");
		 abs_18 = read_abstraction_from_file("abstracciones/5b18.abst");
		 abs_19 = read_abstraction_from_file("abstracciones/5b19.abst");
		 abs_20 = read_abstraction_from_file("abstracciones/5b20.abst");
		 abs_21 = read_abstraction_from_file("abstracciones/5b21.abst");
		 abs_22 = read_abstraction_from_file("abstracciones/5b22.abst");
		 abs_23 = read_abstraction_from_file("abstracciones/5b23.abst");
		 abs_24 = read_abstraction_from_file("abstracciones/5b24.abst");


		 pdb_1 = fopen ("abstracciones/5b(1)_map_state","r");
		 pdb_2 = fopen ("abstracciones/5b(2)_map_state","r");
		 pdb_3 = fopen ("abstracciones/5b(3)_map_state","r");
		 pdb_4 = fopen ("abstracciones/5b(4)_map_state","r");
		 pdb_5 = fopen ("abstracciones/5b(5)_map_state","r");
		 pdb_6 = fopen ("abstracciones/5b(6)_map_state","r");
		 pdb_7 = fopen ("abstracciones/5b(7)_map_state","r");
		 pdb_8 = fopen ("abstracciones/5b(8)_map_state","r");
		 pdb_9 = fopen ("abstracciones/5b(9)_map_state","r");
		 pdb_10 = fopen ("abstracciones/5b(10)_map_state","r");
		 pdb_11 = fopen ("abstracciones/5b(11)_map_state","r");
		 pdb_12 = fopen ("abstracciones/5b(12)_map_state","r");
		 pdb_13 = fopen ("abstracciones/5b(13)_map_state","r");
		 pdb_14 = fopen ("abstracciones/5b(14)_map_state","r");
		 pdb_15 = fopen ("abstracciones/5b(15)_map_state","r");
         pdb_16 = fopen ("abstracciones/5b(16)_map_state","r");
		 pdb_17 = fopen ("abstracciones/5b(17)_map_state","r");
		 pdb_18 = fopen ("abstracciones/5b(18)_map_state","r");
		 pdb_19 = fopen ("abstracciones/5b(19)_map_state","r");
		 pdb_20 = fopen ("abstracciones/5b(20)_map_state","r");
		 pdb_21 = fopen ("abstracciones/5b(21)_map_state","r");
		 pdb_22 = fopen ("abstracciones/5b(22)_map_state","r");
		 pdb_23 = fopen ("abstracciones/5b(23)_map_state","r");
		 pdb_24 = fopen ("abstracciones/5b(24)_map_state","r");

	}
		t_1 = read_state_map(pdb_1);
		t_2 = read_state_map(pdb_2);
		t_3 = read_state_map(pdb_3);
		t_4 = read_state_map(pdb_4);
		t_5 = read_state_map(pdb_5);
		t_6 = read_state_map(pdb_6);
		t_7 = read_state_map(pdb_7);
		t_8 = read_state_map(pdb_8);
		t_9 = read_state_map(pdb_9);
		t_10 = read_state_map(pdb_10);
		t_11 = read_state_map(pdb_11);
		t_12 = read_state_map(pdb_12);
		t_13 = read_state_map(pdb_13);
		t_14 = read_state_map(pdb_14);
		t_15 = read_state_map(pdb_15);
	
        if (!TAM) { 
            t_16 = read_state_map(pdb_16);
		    t_17 = read_state_map(pdb_17);
		    t_18 = read_state_map(pdb_18);
		    t_19 = read_state_map(pdb_19);
		    t_20 = read_state_map(pdb_20);
		    t_21 = read_state_map(pdb_21);
		    t_22 = read_state_map(pdb_22);
		    t_23 = read_state_map(pdb_23);
		    t_24 = read_state_map(pdb_24);
        }

}

void inicializar_pdb_54() {
    if (TAM && VERSION) {
        abs_1 = read_abstraction_from_file("abstracciones/4a51.abst");
		abs_2 = read_abstraction_from_file("abstracciones/4a52.abst");
		abs_3 = read_abstraction_from_file("abstracciones/4a53.abst");
        
        pdb_1 = fopen ("abstracciones/4a5(1)_map_state","r");
		pdb_2 = fopen ("abstracciones/4a5(2)_map_state","r");
		pdb_3 = fopen ("abstracciones/4a5(3)_map_state","r");
        
        t_1 = read_state_map(pdb_1);
		t_2 = read_state_map(pdb_2);
		t_3 = read_state_map(pdb_3);
    }
    else if (VERSION && !TAM) {
        abs_1 = read_abstraction_from_file("abstracciones/5a41.abst");
		abs_2 = read_abstraction_from_file("abstracciones/5a42.abst");
		abs_3 = read_abstraction_from_file("abstracciones/5a43.abst");
        abs_4 = read_abstraction_from_file("abstracciones/5a44.abst");
		abs_5 = read_abstraction_from_file("abstracciones/5a45.abst");
		abs_6 = read_abstraction_from_file("abstracciones/5a46.abst");
        
        pdb_1 = fopen ("abstracciones/5a4(1)_map_state","r");
		pdb_2 = fopen ("abstracciones/5a4(2)_map_state","r");
		pdb_3 = fopen ("abstracciones/5a4(3)_map_state","r");
        pdb_4 = fopen ("abstracciones/5a4(4)_map_state","r");
		pdb_5 = fopen ("abstracciones/5a4(5)_map_state","r");
		pdb_6 = fopen ("abstracciones/5a4(6)_map_state","r");
        
        t_1 = read_state_map(pdb_1);
		t_2 = read_state_map(pdb_2);
		t_3 = read_state_map(pdb_3);
        t_4 = read_state_map(pdb_4);
		t_5 = read_state_map(pdb_5);
		t_6 = read_state_map(pdb_6);
    }
    else if (!VERSION && TAM) {
        abs_1 = read_abstraction_from_file("abstracciones/4b51.abst");
		abs_2 = read_abstraction_from_file("abstracciones/4b52.abst");
		abs_3 = read_abstraction_from_file("abstracciones/4b53.abst");
        
        pdb_1 = fopen ("abstracciones/4b5(1)_map_state","r");
		pdb_2 = fopen ("abstracciones/4b5(2)_map_state","r");
		pdb_3 = fopen ("abstracciones/4b5(3)_map_state","r");
        
        t_1 = read_state_map(pdb_1);
		t_2 = read_state_map(pdb_2);
		t_3 = read_state_map(pdb_3);

    }
    
    else {
        abs_1 = read_abstraction_from_file("abstracciones/5b41.abst");
    	abs_2 = read_abstraction_from_file("abstracciones/5b42.abst");
		abs_3 = read_abstraction_from_file("abstracciones/5b43.abst");
        abs_4 = read_abstraction_from_file("abstracciones/5b44.abst");
		abs_5 = read_abstraction_from_file("abstracciones/5b45.abst");
		abs_6 = read_abstraction_from_file("abstracciones/5b46.abst");
        
        pdb_1 = fopen ("abstracciones/5b4(1)_map_state","r");
		pdb_2 = fopen ("abstracciones/5b4(2)_map_state","r");
		pdb_3 = fopen ("abstracciones/5b4(3)_map_state","r");
        pdb_4 = fopen ("abstracciones/5b4(4)_map_state","r");
		pdb_5 = fopen ("abstracciones/5b4(5)_map_state","r");
		pdb_6 = fopen ("abstracciones/5b4(6)_map_state","r");
        
        t_1 = read_state_map(pdb_1);
		t_2 = read_state_map(pdb_2);
		t_3 = read_state_map(pdb_3);
        t_4 = read_state_map(pdb_4);
		t_5 = read_state_map(pdb_5);
		t_6 = read_state_map(pdb_6);

    }

}


int heuristica(state_t *inicio, bool manhatthan) {
	state_t abstracto;
	int h = 0;
    if (manhatthan) {
        if (TAM) { 
    		abstract_state(abs_1,inicio,&abstracto);
        	h += *state_map_get(t_1,&abstracto);
    		abstract_state(abs_2,inicio,&abstracto);
        	h += *state_map_get(t_2,&abstracto);
    		abstract_state(abs_3,inicio,&abstracto);
        	h += *state_map_get(t_3,&abstracto);
    		abstract_state(abs_4,inicio,&abstracto);
        	h += *state_map_get(t_4,&abstracto);
    		abstract_state(abs_5,inicio,&abstracto);
        	h += *state_map_get(t_5,&abstracto);
    		abstract_state(abs_6,inicio,&abstracto);
        	h += *state_map_get(t_6,&abstracto);
    		abstract_state(abs_7,inicio,&abstracto);
        	h += *state_map_get(t_7,&abstracto);
    		abstract_state(abs_8,inicio,&abstracto);
        	h += *state_map_get(t_8,&abstracto);
    		abstract_state(abs_9,inicio,&abstracto);
        	h += *state_map_get(t_9,&abstracto);
    		abstract_state(abs_10,inicio,&abstracto);
        	h += *state_map_get(t_10,&abstracto);
    		abstract_state(abs_11,inicio,&abstracto);
        	h += *state_map_get(t_11,&abstracto);
    		abstract_state(abs_12,inicio,&abstracto);
        	h += *state_map_get(t_12,&abstracto);
    		abstract_state(abs_13,inicio,&abstracto);
    	    h += *state_map_get(t_13,&abstracto);
    		abstract_state(abs_14,inicio,&abstracto);
    	    h += *state_map_get(t_14,&abstracto);
    		abstract_state(abs_15,inicio,&abstracto);
    	    h += *state_map_get(t_15,&abstracto);
	    	return h;
        }
        else { 
            abstract_state(abs_1,inicio,&abstracto);
        	h += *state_map_get(t_1,&abstracto);
    		abstract_state(abs_2,inicio,&abstracto);
        	h += *state_map_get(t_2,&abstracto);
    		abstract_state(abs_3,inicio,&abstracto);
        	h += *state_map_get(t_3,&abstracto);
    		abstract_state(abs_4,inicio,&abstracto);
        	h += *state_map_get(t_4,&abstracto);
    		abstract_state(abs_5,inicio,&abstracto);
        	h += *state_map_get(t_5,&abstracto);
    		abstract_state(abs_6,inicio,&abstracto);
        	h += *state_map_get(t_6,&abstracto);
    		abstract_state(abs_7,inicio,&abstracto);
        	h += *state_map_get(t_7,&abstracto);
    		abstract_state(abs_8,inicio,&abstracto);
        	h += *state_map_get(t_8,&abstracto);
    		abstract_state(abs_9,inicio,&abstracto);
        	h += *state_map_get(t_9,&abstracto);
    		abstract_state(abs_10,inicio,&abstracto);
        	h += *state_map_get(t_10,&abstracto);
    		abstract_state(abs_11,inicio,&abstracto);
        	h += *state_map_get(t_11,&abstracto);
    		abstract_state(abs_12,inicio,&abstracto);
        	h += *state_map_get(t_12,&abstracto);
    		abstract_state(abs_13,inicio,&abstracto);
    	    h += *state_map_get(t_13,&abstracto);
    		abstract_state(abs_14,inicio,&abstracto);
    	    h += *state_map_get(t_14,&abstracto);
    		abstract_state(abs_15,inicio,&abstracto);
    	    h += *state_map_get(t_15,&abstracto);
            abstract_state(abs_16,inicio,&abstracto);
        	h += *state_map_get(t_16,&abstracto);
    		abstract_state(abs_17,inicio,&abstracto);
        	h += *state_map_get(t_17,&abstracto);
    		abstract_state(abs_18,inicio,&abstracto);
        	h += *state_map_get(t_18,&abstracto);
    		abstract_state(abs_19,inicio,&abstracto);
        	h += *state_map_get(t_19,&abstracto);
    		abstract_state(abs_20,inicio,&abstracto);
        	h += *state_map_get(t_20,&abstracto);
    		abstract_state(abs_21,inicio,&abstracto);
        	h += *state_map_get(t_21,&abstracto);
    		abstract_state(abs_22,inicio,&abstracto);
        	h += *state_map_get(t_22,&abstracto);
    		abstract_state(abs_23,inicio,&abstracto);
    	    h += *state_map_get(t_23,&abstracto);
    		abstract_state(abs_24,inicio,&abstracto);
    	    h += *state_map_get(t_24,&abstracto);
	    	return h;
        }
	}
	else {
		if (TAM) {
            abstract_state(abs_1,inicio,&abstracto);
        	h += *state_map_get(t_1,&abstracto);
    		abstract_state(abs_2,inicio,&abstracto);
        	h += *state_map_get(t_2,&abstracto);
    		abstract_state(abs_3,inicio,&abstracto);
        	h += *state_map_get(t_3,&abstracto);
            return h;
        }
        else {
            abstract_state(abs_1,inicio,&abstracto);
        	h += *state_map_get(t_1,&abstracto);
    		abstract_state(abs_2,inicio,&abstracto);
        	h += *state_map_get(t_2,&abstracto);
    		abstract_state(abs_3,inicio,&abstracto);
        	h += *state_map_get(t_3,&abstracto);
    		abstract_state(abs_4,inicio,&abstracto);
        	h += *state_map_get(t_4,&abstracto);
    		abstract_state(abs_5,inicio,&abstracto);
        	h += *state_map_get(t_5,&abstracto);
    		abstract_state(abs_6,inicio,&abstracto);
        	h += *state_map_get(t_6,&abstracto);
            return h;
        }
    }
}

Estado * addd(state_t * inicio, bool manhatthan) {

    state_map_t *tabla_hash = new_state_map();
    state_map_t *tabla_distancias = new_state_map();
    priority_queue<Estado*,vector<Estado*>,EstadoComp> nodos;
    nodos.push(new Estado(inicio,init_history,NULL,heuristica(inicio,manhatthan)));
    state_map_add(tabla_hash,inicio,GRIS);
    state_map_add(tabla_distancias,inicio,0);
    Estado *p;
   	state_t *hijo;
	ruleid_iterator_t iterator;
    int *color;
    int ruleid;
    int dist;
    nodos_generados = 1;

    while (! nodos.empty()) {
    		p = nodos.top();
    		nodos.pop();
            if (is_goal(p->get_estado())) { 
                return p;
                break;
            }

        	init_fwd_iter(&iterator,p->get_estado());
    		while ( (ruleid = next_ruleid(&iterator)) >=0 ) {
    			    if (! fwd_rule_valid_for_history(p->get_historia(),ruleid) )
    					continue;
    				hijo = (state_t *) malloc (sizeof(state_t));
    				dist = *state_map_get(tabla_distancias,p->get_estado()) + get_fwd_rule_cost(ruleid);
                    apply_fwd_rule(ruleid,p->get_estado(),hijo);
                    color = state_map_get(tabla_hash,hijo);
                    if ( color == NULL || (dist < *state_map_get(tabla_distancias,hijo))) {
                        state_map_add(tabla_hash,hijo,GRIS);
                        state_map_add(tabla_distancias,hijo,dist);
                 		nodos.push(new Estado(hijo, next_fwd_history(p->get_historia(),ruleid),p,dist+heuristica(hijo,manhatthan)));
                        nodos_generados += 1;
                    }
    		}
            state_map_add(tabla_hash,p->get_estado(),NEGRO);
	}
}


Estado * ida (state_t * inicio, bool manhatthan) {

    Estado *p;
   	state_t *hijo;
	ruleid_iterator_t iterator;
    int ruleid;
    int dist;
    int bound = heuristica(inicio,manhatthan);
    p = new Estado(inicio,init_history,NULL);
    Par res;
    nodos_generados = 1;

    while (true) {
        res = f_bound_dfs(p,bound,manhatthan);
        if (res.Nodo != NULL ) 
            return res.Nodo;
        bound = res.distancia;
    }
}

Par f_bound_dfs (Estado *inicio ,int bound, bool manhatthan) {

    Estado *p;
   	state_t *hijo;
	ruleid_iterator_t iterator;
    int ruleid;
    int dist = -1;
    int distancia;
    Par res;
	Estado *hijoE;

    if ((inicio->get_distancia() + heuristica(inicio->get_estado(),manhatthan)) > bound){
        res.Nodo = NULL;
        res.distancia = inicio->get_distancia() + heuristica(inicio->get_estado(),manhatthan);
        return res;
    }

    if (is_goal(inicio->get_estado())){
        res.Nodo = inicio;
        res.distancia = inicio->get_distancia();
        return res;
     }

   	init_fwd_iter(&iterator,inicio->get_estado());
    while ( (ruleid = next_ruleid(&iterator)) >=0 ) {
         if (! fwd_rule_valid_for_history(inicio->get_historia(),ruleid) )
        			continue;
  	    hijo = (state_t *) malloc (sizeof(state_t));
        apply_fwd_rule(ruleid,inicio->get_estado(),hijo);
        distancia = inicio->get_distancia() + get_fwd_rule_cost(ruleid);
        hijoE = new Estado(hijo,next_fwd_history(inicio->get_historia(),ruleid),inicio,distancia);
        nodos_generados += 1;
        res = f_bound_dfs(hijoE,bound,manhatthan);
        if (res.Nodo != NULL) 
            return res;
        if (dist < 0) {
            res.Nodo = NULL;
            dist = res.distancia;
        }
        else if (dist > res.distancia) {
            dist = res.distancia;
        }

        free(hijo);
        delete (hijoE);
    
    }
    res.Nodo = NULL;
    res.distancia = dist;
    return res; 
}



void imprimir( ofstream *file, Estado * p) {     
    Pila pila;
    char string[100];
    while ( p->get_padre() != NULL) {
        pila.push(p);
        p = p->get_padre();
    }
    pila.push(p);
    *file << "pasos para resolverlo: \n" << endl;
    while (!pila.empty()) {

        sprint_state ( &string[0],100,pila.top()->get_estado());
        pila.pop();
        *file << string << endl;
    }
}


void ayuda(char * nombre) {
	cout << PRG_NAME << ": modo de uso" << endl ;
	cout << PRG_NAME <<" -f [archivo] -h -i -e -p -o" << endl ;
	cout << "Opciones: \n-f [archivo]: indica que en 'archivo' se guardaran los datos" << endl;
	cout << "-h : muestra esta ayuda" << endl;
    cout << "-i : Indica el estado inicial" << endl;
	cout << "-e : Indica el archivo donde se leera el estado inicial" << endl;
    cout << "Si se usa al mismo tiempo '-e' y '-i', el archivo tiene preferencia" << endl;
	cout << "-p : Se usara las PDB de 555 (o 444444 si es 24-puzzle), en lugar de distancia manhatthan " << endl;
    cout << "-o : Se usara IDA* en lugar de A* con DDD "<< endl;
	
	exit(0);
}
