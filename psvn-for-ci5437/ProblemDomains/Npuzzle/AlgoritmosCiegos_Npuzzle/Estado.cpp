// Fichero de definicion de la Clase Estado
# include "Estado.hpp"
Estado::Estado ( state_t *est, int his, Estado *p, int d)  { 
	estado = est;
	historia = his; 
    padre = p;
    distancia = d;
};

Estado::Estado ( state_t *est, int his, Estado *p)  { 
	estado = est;
	historia = his; 
    padre = p;
    distancia = 0;
};

Estado::Estado () { 
}; 
// Constructor
state_t * Estado::get_estado() {
	return estado;
};
// Devuelve el estado
int Estado::get_historia() {
	return historia;
};
//Devuelve la historia
Estado * Estado::get_padre() {
    return padre;
};
int Estado::get_distancia() {
    return distancia;
};
void Estado::set_distancia(int d) {
	distancia=d;
};
void Estado::set_padre(Estado *p) {
	padre = p;
};
void Estado::set_estado(state_t *e){
	estado = e;
};
void Estado::set_historia(int h) {
	historia = h;
};
