// Fichero de definicion de la Clase Estado
#ifndef _clase_estado_
#define _clase_estado_


class Estado {
	private:
		state_t *estado;
		int historia;
        Estado *padre;
        int distancia;
	public:
		Estado (state_t *est, int his, Estado *p, int d);
        Estado (state_t *est, int his, Estado *p);
		// Constructor
		Estado ();
		state_t * get_estado();
		// Devuelve el estado
		int get_historia();
		//Devuelve la historia
        Estado * get_padre();
        int get_distancia();
		void set_distancia(int d);
		void set_padre( Estado *p);
		void set_estado( state_t *e);
		void set_historia(int h);
};

struct EstadoComp {
	bool operator()(Estado * e1, Estado * e2) {
		return (e1->get_distancia() > e2->get_distancia());
	}
};

typedef struct {
	Estado * Nodo;
	int distancia;
} Par;

#endif
