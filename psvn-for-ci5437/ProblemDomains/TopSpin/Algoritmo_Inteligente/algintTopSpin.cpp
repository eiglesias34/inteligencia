/*
 * Autor: Enrique Iglesias y Jesus Parra
 * Fecha: 
 */

#include <iostream>
#include <queue>
#include <vector>
#include <unistd.h>
#include <cstddef>
#include <fstream>
#include <string>
#include "Par.hpp"
using namespace std;

abstraction_t *abs_1;
abstraction_t *abs_2;
abstraction_t *abs_3;

FILE *pdb_1;
FILE *pdb_2;
FILE *pdb_3;

state_map_t * t_1;
state_map_t * t_2;
state_map_t * t_3;

state_map_t *tabla_hash2 = new_state_map();

void Cargar(int op){
	if (op == 0)
	{
		abs_1 = read_abstraction_from_file("abstracciones/topspin12par.abst");
		abs_2 = read_abstraction_from_file("abstracciones/topspin12impar.abst");

		pdb_1 = fopen ("abstracciones/topspin12par_map_state","r");
		if (pdb_1 ==NULL ) {
				cout << "Auxilio" << endl;
				exit(-1);
			};
		pdb_2 = fopen ("abstracciones/topspin12impar_map_state","r");
	} 
	else if (op == 1)
	{
		abs_1 = read_abstraction_from_file("abstracciones/topspin14par.abst");
		abs_2 = read_abstraction_from_file("abstracciones/topspin14impar.abst");

		pdb_1 = fopen ("abstracciones/topspin14par_map_state","r");
		pdb_2 = fopen ("abstracciones/topspin14impar_map_state","r");
	}
	else if (op == 2)
	{
		abs_1 = read_abstraction_from_file("abstracciones/topspin16par2.abst");
		abs_2 = read_abstraction_from_file("abstracciones/topspin162.abst");
		abs_3 = read_abstraction_from_file("abstracciones/topspin163.abst");

		pdb_1 = fopen ("abstracciones/topspin16par2_map_state","r");
		pdb_2 = fopen ("abstracciones/topspin162_map_state","r");
		pdb_3 = fopen ("abstracciones/topspin163_map_state","r");

		t_3 = read_state_map(pdb_3);
	}
	else if (op == 3)
	{
		abs_1 = read_abstraction_from_file("abstracciones/topspin171.abst");
		abs_2 = read_abstraction_from_file("abstracciones/topspin172.abst");
		abs_3 = read_abstraction_from_file("abstracciones/topspin173.abst");

		pdb_1 = fopen ("abstracciones/topspin171_map_state","r");
		pdb_2 = fopen ("abstracciones/topspin172_map_state","r");
		pdb_3 = fopen ("abstracciones/topspin173_map_state","r");

		t_3 = read_state_map(pdb_3);
	}

	t_1 = read_state_map(pdb_1);
	t_2 = read_state_map(pdb_2);
};

int PDB (state_t* state,int op) {
	int heu = 0;
	state_t abstracto;

	abstract_state(abs_1,state,&abstracto);
    heu += *state_map_get(t_1,&abstracto);

	abstract_state(abs_2,state,&abstracto);
    heu += *state_map_get(t_2,&abstracto);
    
    if ((op == 2) || (op == 3))
    {
    	abstract_state(abs_3,state,&abstracto);
    	heu += *state_map_get(t_3,&abstracto);
    };
	return heu;
};

Par a_asterisco(state_t* state,int op){
	Par p;
	p.set_state(state);
	p.set_heu(PDB(state,op));
	state_t* child;
	Par n;
	ruleid_iterator_t iter;
	state_map_t *tabla_hash = new_state_map();
	state_map_t *tabla_distancias = new_state_map();
	state_map_t *tabla_historias = new_state_map();
	state_map_add(tabla_hash,state,0);
	state_map_add(tabla_distancias,state,0);
	state_map_add(tabla_historias,state,init_history);
	int ruleid;
	int *color;
	int dist;
	priority_queue < Par > pos;
	pos.push(p);
	while(!pos.empty()){

		n = pos.top();
		pos.pop();
		
		if (is_goal(n.get_state()))
		{
			n.set_dist(*state_map_get(tabla_distancias,n.get_state()));
			return n;
		};
		
		init_fwd_iter(&iter, n.get_state());
		while(( ruleid = next_ruleid( &iter ) ) >= 0 ){
			child = (state_t*) malloc (sizeof(state_t));
			apply_fwd_rule( ruleid, n.get_state(), child );
			if ((fwd_rule_valid_for_history(*state_map_get(tabla_historias,n.get_state()), ruleid) != 0))
			{
				color = state_map_get(tabla_hash,child);
				dist = *state_map_get(tabla_distancias,n.get_state()) + get_fwd_rule_cost(ruleid);
				if ((color == NULL)||(dist < *state_map_get(tabla_distancias,child)))
				{
					if (PDB(child,op) > 0)
					{
						p.set_state(child);
						p.set_heu(PDB(child,op));
						pos.push(p);
						state_map_add(tabla_distancias,child,dist);
						state_map_add(tabla_hash,child,0);
						state_map_add(tabla_historias,child,(next_fwd_history(*state_map_get(tabla_historias,n.get_state()), ruleid)));
					};
				};
			};
		};
		state_map_add(tabla_hash,n.get_state(),1);
	};
};

Par f_bounded_nfs(state_t* state,int g, int bound, int hist,int op,int dist,state_map_t* tabla){
	ruleid_iterator_t iter;
    int ruleid; 
    state_t* child;
    Par p;
    int hist2;
    int *color;
	int f = get_fwd_rule_cost(g) + PDB(state,op);
	state_map_add(tabla,state,0);
	if (f > bound)
	{
	 	p.set_state(NULL);
	 	p.set_heu(f);
	 	p.set_dist(0);
	 	return p;
	};
	if (is_goal(state))
	{ 
	 	p.set_state(state);
	 	p.set_heu(0);
	 	p.set_dist(dist);
		return p;
	}
	int t = -1;
	init_fwd_iter(&iter, state);
	while ( ( ruleid = next_ruleid( &iter ) ) >= 0 ){
		child = (state_t*) malloc (sizeof(state_t));
        apply_fwd_rule( ruleid, state, child );
        color = state_map_get(tabla,child);
        if ((fwd_rule_valid_for_history(hist, ruleid) != 0) && (color == NULL))
		{
			hist2 = next_fwd_history(hist, ruleid);
			p = f_bounded_nfs(child,ruleid,bound,hist2,op,dist+1,tabla);
			if (p.get_state() != NULL)
			{
				return p;
			}
			if (t < 0)
			{
				t = p.get_heu();
			}
			else{
				if (p.get_heu() > t)
				{
					t = p.get_heu();
				};
			};
		};
	};
	p.set_state(NULL);
	p.set_heu(t);
	p.set_dist(dist);
	return p; 
};

Par ida_asterisico(state_t* state, int op){
	int bound = PDB(state,op);
	state_map_t *tabla_hash = new_state_map();
	while(true){
		Par p = f_bounded_nfs(state,0,bound,init_history,op,0,tabla_hash);
		if (p.get_state() != NULL)
		{
			return p;
		};
		bound = p.get_heu();
	};
};

int main(int argc, char const *argv[])
{
	clock_t comienzo, final;
    double segundos;
	state_t* state;
	string line;
	char problem[100];
	ofstream file;
	Par p;
	ifstream myfile (argv[3]);
  	if (myfile.is_open())
  	{
  		file.open(argv[4]);
  		while ( getline (myfile,line) ){
    		state = (state_t*) malloc (sizeof(state_t));
    		if ( (strcpy(problem,line.c_str())) == NULL ) {
                cout << "Fallo al leer el estado inicial" << endl;
                exit(-1);
           	};
            if (read_state (problem,state) <=0 ) {
                cout << "Fallo al leer el estado inicial" << endl;
                exit(-1);
            };
            comienzo = clock();
            if (atoi(argv[1]) == 0)
	  		{
	  			printf("A*\n");
	  			Cargar(atoi(argv[2]));
	    		p = a_asterisco(state,atoi(argv[2]));
	  		}
	  		else if (atoi(argv[1]) == 1)
	  		{
	  			printf("IDA*\n");
	  			Cargar(atoi(argv[2]));
	    		p = ida_asterisico(state,atoi(argv[2]));
	  		}
	  		else{
	  			cout <<"Parámetro no valido" ;
	  			exit(1);
	  		};
	  		final = clock();
	  		segundos = (double) (final - comienzo) / (double)CLOCKS_PER_SEC;
	  		file << line << " : " << PDB(state,atoi(argv[2])) <<" " << p.get_dist() << " ";
	  		file << " " << segundos << endl;
        };
        file.close();
        myfile.close();
    };
	return 0;
}