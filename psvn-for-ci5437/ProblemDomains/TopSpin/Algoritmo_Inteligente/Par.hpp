#ifndef _clase_par_
#define _clase_par_
class Par
{
private:
	state_t* state;
	int heu;
	int dist;
public:
	Par(state_t *state, int heu);
	Par();
	state_t* get_state();
	int get_heu();
	int get_dist();
	void set_state(state_t* state);
	void set_heu(int heu);
	void set_dist(int d);
	bool operator < (const Par &rhs) const
	{
		return (heu < rhs.heu);
	};
};
#endif