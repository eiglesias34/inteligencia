El codigo para correr los algorimos ciegos es algintTopSpin.cpp.
Para ejecutar el codigo es necesario pasarle cuatro parametros. 
El primero es el algoritmo que desea, los cuales se organizan
de esta forma:
0:A*
1:IDA*
El segundo es el tamano del juego:
0: 12
1: 14
2: 16
3: 17

El tercero es el archivo con los casos de prueba.
El cuarto es el nombre del archivo a crear con los resultados de la corrida. 

Los archivos de texto presentes en la carpeta continen los casos de prueba corridos y los resultados obtenidos
luego de correr cada algoritmo.