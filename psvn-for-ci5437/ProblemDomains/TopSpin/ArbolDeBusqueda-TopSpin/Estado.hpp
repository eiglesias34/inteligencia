// Fichero de definicion de la Clase Estado
#ifndef _clase_estado_
#define _clase_estado_


class Estado {
	private:
		state_t *estado;
		int historia;
	public:
		Estado (state_t *est, int his);
		// Constructor
		Estado ();
		state_t * get_estado();
		// Devuelve el estado
		int get_historia();
		//Devuelve la historia
};
#endif
