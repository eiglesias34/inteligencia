/*
 * Autor: Enrique Iglesias y Jesus Parra
 * Fecha: 
 */

#include <iostream>
#include <queue>
#include <fstream>
#include <string>
#include <unistd.h>
#include <cstddef>
#include <cstring>
#include <vector>
using namespace std;

int BFS(state_t* state){
	state_map_t *tabla_hash = new_state_map();
	state_map_t *tabla_historias = new_state_map();
	state_map_add(tabla_hash,state,0);
	state_map_add(tabla_historias,state,init_history); 
	state_t* child;
	state_t* n;
	priority_queue<state_t*> q;
	ruleid_iterator_t iter;
    int ruleid;
    int nodo = 1;
    int *color; 
	q.push(state); 
	while (! q.empty() ){
		n = q.top();
		q.pop();
		if (is_goal(n))
		{
			return nodo;
		};
        init_fwd_iter(&iter, n);
        while ( ( ruleid = next_ruleid( &iter ) ) >= 0 )
        {
            if ((fwd_rule_valid_for_history(*state_map_get(tabla_historias,n), ruleid) != 0))
            {
            	child = (state_t*) malloc (sizeof(state_t));
            	apply_fwd_rule( ruleid, n, child );
            	color = state_map_get(tabla_hash,child);
            	if ( color == NULL || *color == 0) {
            		q.push(child);
            		state_map_add(tabla_historias,child,(next_fwd_history(*state_map_get(tabla_historias,n), ruleid)));
            	};
            };
        };
        state_map_add(tabla_hash,n,1);
        nodo++;
	};
};

int ucs(state_t* state){
	state_t* child;
	state_t* n;
	int nodo = 1;
	state_map_t *tabla_hash = new_state_map();
	state_map_t *tabla_distancias = new_state_map();
	state_map_t *tabla_historias = new_state_map();
	ruleid_iterator_t iter;
    int ruleid; 
	priority_queue<state_t*> q;
	int *color;
	int dist;
	q.push(state);
	state_map_add(tabla_distancias,state,0);
	state_map_add(tabla_hash,state,0);
	state_map_add(tabla_historias,state,init_history);
	while (! q.empty() ){
		n = q.top();
		q.pop();
		if (is_goal(n)){
			return nodo;
		};
		init_fwd_iter(&iter, n);
		while ( ( ruleid = next_ruleid( &iter ) ) >= 0 ){
			if ((fwd_rule_valid_for_history(*state_map_get(tabla_historias,n), ruleid) != 0))
			{	
				child = (state_t*) malloc (sizeof(state_t));
        		apply_fwd_rule( ruleid, n, child );
        		color = state_map_get(tabla_hash,child);
				dist = *state_map_get(tabla_distancias,n) + get_fwd_rule_cost(ruleid);
				if ( color == NULL || (dist < *state_map_get(tabla_distancias,child))) 
				{
					q.push(child);
					state_map_add(tabla_distancias,child,dist);
					state_map_add(tabla_hash,child,0);
					state_map_add(tabla_historias,child,(next_fwd_history(*state_map_get(tabla_historias,n), ruleid)));
				};
			};
		};
		state_map_add(tabla_hash,n,1);
		nodo++;
	};
};

int bound_dfs(state_t* node,int d, int bound, int nodos){
	state_t* child;
	int m;
	ruleid_iterator_t iter;
    int ruleid;
    nodos++; 
	if (d > bound)
	{
		return 0;
	};
	if (is_goal(node)){
			return nodos;
	};
	init_fwd_iter(&iter, node);
	while ( ( ruleid = next_ruleid( &iter ) ) >= 0 ){
		child = (state_t*) malloc (sizeof(state_t));
        apply_fwd_rule( ruleid, node, child );
		m = bound_dfs(child,d+1,bound,nodos);
		if (m != 0)
		{
			return m;
		};
		free(child);
	};
};

int dfid(state_t* state){
	int n;
	int bound = 0;
	while(true){
		n = bound_dfs(state,0,bound,1);
		if (n != 0)
		{
			
			return n;
		};
		bound = bound + 1;
	};
};

int main(int argc, char const *argv[])
{
	clock_t comienzo, final;
    double segundos;
	state_t* state;
	string line;
	char problem[100];
	int n;
	ofstream file;
	ifstream myfile;
	myfile.open(argv[2], ios::in);
  	if (myfile.is_open())
  	{
  		file.open(argv[3],ios::out);
  		while ( getline (myfile,line) ){
    		state = (state_t*) malloc (sizeof(state_t));
    		if ( (strcpy(problem,line.c_str())) == NULL ) {
                cout << "Fallo al leer el estado inicial" << endl;
                exit(-1);
           	};
            if (read_state (problem,state) <=0 ) {
                cout << "Fallo al leer el estado inicial" << endl;
                exit(-1);
            };
            comienzo = clock();
	  		if (atoi(argv[1]) == 0)
	  		{
	  			printf("BFS\n");
	    		n = BFS(state);
	  		}
	  		else if (atoi(argv[1]) == 1)
	  		{
	  			printf("UCS\n");
	    		n = ucs(state);
	  		}
	  		else if (atoi(argv[1]) == 2)
	  		{
	  			printf("DFID\n");
	    		n = dfid(state);
	  		}
	  		else{
	  			cout <<"Parámetro no valido" ;
	  			exit(1);
	  		};
	  		final = clock();
	  		segundos = (double) (final - comienzo) / (double)CLOCKS_PER_SEC;
	  		file << line << " : " << "-" <<" " << n << " ";
	  		file << " " << segundos << endl;
    	};
    	file.close();
    	myfile.close();
  	}
  	else {cout << "Unable to open file"; }
	return 0;
}