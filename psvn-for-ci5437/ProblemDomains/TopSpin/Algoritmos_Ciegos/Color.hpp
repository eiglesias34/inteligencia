#ifndef _clase_color_
#define _clase_color_
#include <cstring>

class Color
{
	private:
		state_t *state;
		int color;
		int dist;
	public:
		Color(state_t *state, int color);
		Color();
		state_t* get_state();
		int get_color();
		int get_dist();
		void set_color(int color);
		void set_state(state_t* state);
		void set_dist(int dist);
		bool operator < (const Color &rhs) const
		{
			return (dist < rhs.dist);
		};
};

#endif