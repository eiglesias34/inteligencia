El codigo para correr los algorimos ciegos es algTopSpin.cpp.
Para ejecutar el codigo es necesario pasarle tres parametros. 
El primero es el algoritmo que desea, los cuales se organizan
de esta forma:
0:BFS
1:UCS
2:DFID
El segundo es el archivo con los casos de prueba.
El tercero es el nombre del archivo a crear con los resultados de la corrida. 

Los archivos de texto presentes en la carpeta continen los casos de prueba corridos y los resultados obtenidos
luego de correr cada algoritmo. 