#include <iostream>
#include <fstream>
#include <queue> 
#include <unistd.h>
#include "Estado.hpp"

using namespace std;
typedef queue<Estado*> Cola;

void ayuda() {
	cout << "TorreDeHanoi: modo de uso" << endl ;
	cout << "TorreDeHanoi -f [archivo] -p -h -n [nivel]" << endl ;
	cout << "Opciones: \n-f [archivo]: indica que en 'archivo' se guardaran los datos" << endl;
	cout << "-p : Indica si quiere usar parent pruning " << endl;
	cout << "-h : muestra esta ayuda" << endl;
	cout << "-n [nivel] : Indica el nivel maximo que se buscara, por defecto 1"<< endl;
	exit(0);
}

int main(int argc, char *argv[]) {
	Cola Cola_padre;
	Cola Cola_hijos;
	int nivel = 0;
	long long unsigned nodos = 1;
    long long unsigned nodos_anterior=1;
    float factor;
	int goal;
	state_t meta;
	first_goal_state(&meta, &goal);
	Estado *p;
	state_t *hijo;
	ruleid_iterator_t iterator;
	int ruleid;
	char opcion;
	ofstream file;
	bool parent = false;
	int niveluser = 1;

	if (argc < 3) {
		cout << "Necesita la menos el archivo donde se guardaran los datos" << endl;
		ayuda();
	}


	while ((opcion = getopt(argc,argv,"f:phn:")) != -1 ) {
		switch (opcion) {
			case 'h' :	ayuda();
				break;
			case 'f':	file.open(optarg);
				break;
			case 'p':  parent = true;
				break;
			case 'n':  niveluser = atoi(optarg);
				break;
			default:  ayuda();
				break;
			}
	}				

	if (file.fail() || !(file.is_open())) {
		cout << "Fallo al abrir el archivo, se detiene la ejecucion" << endl;
		exit(-1);
	}
	Cola_padre.push(new Estado(&meta,init_history));
	int i = 0;
	while (i < niveluser) {
		nodos = 0;
		while (! Cola_padre.empty()) {
			p = Cola_padre.front();
			Cola_padre.pop();
			init_fwd_iter(&iterator,p->get_estado());
			while ( (ruleid = next_ruleid(&iterator)) >=0 ) {
					if (parent) {
						if (! fwd_rule_valid_for_history(p->get_historia(),ruleid) )
							continue;
					}
					hijo = (state_t *) malloc (sizeof(state_t));
					apply_fwd_rule(ruleid,p->get_estado(),hijo);
					Cola_hijos.push(new Estado(hijo, next_fwd_history(p->get_historia(),ruleid)));
					nodos++;
			}
		}
		
        factor =  (float) nodos / nodos_anterior; 
   		file << "nivel : " << nivel << " ; nodos: " << nodos_anterior << " ; factor de ramificacion: " << factor << endl ;
        nivel++;
        nodos_anterior = nodos;
		Cola_padre = Cola_hijos;
		Cola_hijos = Cola();
		i++;
	}
	file.close();
}

 
